## Before you start

Make sure you have Docker installed (https://docs.docker.com/get-docker/)

### Setup

Start the installation and the container by running:
    
    docker-compose up --build

### Run

When restarting the container, consider removing the --build flag:

    docker-compose up

### Development

Go to http://127.0.0.1:8000/api and start developing. 

Don't worry, you don't need to restart the container after every change. The container reloads automatically.

### Troubleshoot Docker

Stop all containers
    
    docker stop $(docker ps -a -q)
    
Remove all containers

    docker rm $(docker ps -a -q) 
    
Error "Cannot create container for service db: invalid volume specification"

    docker-compose rm -v 
    docker-compose up --build
    
In case the php service can't connect to the database, try to start the database container manually

    docker-compose up db 

When the database is ready to accept connections
 
    docker-compose up php  

## Case

This API provides the backend for a super simple Blog. A User can create a post with a title and some content. 
To keep things simple we don't use any authentication.
The API Platform (https://api-platform.com/) takes care of generating the required endpoints for us.

The backend is missing a comment feature. Users should be able to comment on posts. A comment contains:
  * Creation date
  * Ref to the user
  * Text with the comment

Whenever a new comment is added, the creator of the post should receive a simple notification via email. 

Our imaginary frontend allows also to mention other users by setting a reference in the text. An example: 

    Hi [@chw]: what do you think about that topic?

Whereby `chw` is the username of the mentioned user. 
Every mentioned user receives a notification via email as well.

### Tasks

There is no time limit to solve the requirements. 
But we should have given you a date, until that we are expecting the solution to be finished and delivered.

**0. Start**

Fork this repository to your prefered git provider. 
Create a new branch and commit your solution in that branch.

**1. Entity and endpoint**

Create the required entity and endpoints, so new comments can be created on a post.

**2. Notification**

When a new comment is created send a notification to the creator of the post and all mentioned users.

Subject: New comment on post "$title"   
Body: There is a new comment on post "$title", check it out!

**3. Tests**

Write some unit tests to cover the basic functionality. 
For example the creator of a post should not receive a notification twice if he gets mentioned in a post.

**4. Pull request**

Create a pull requests with your changes and send us the link to that pull request.

### Bonus

Make the email sending and comment parsing executed asynchronously by using the Symonfy Messenger (https://api-platform.com/docs/core/messenger/).
